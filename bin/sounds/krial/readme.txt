ADDRESS: http://opengameart.org/content/siclone-sound-effects
AUTHORS: krial
LICENCE: CC-0 (Public Domain)

A few simple sound effects for a simple GameMaker: Studio tutorial on making a
sort of Space Invaders clone (Siclone) using public domain assets.

Created as I needed them using LabChirp.
