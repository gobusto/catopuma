# C A T O P U M A   D E M O
#
# Copyright (c) 2016, Thomas Glyn Dennis
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
# REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
# AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
# INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
# LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
# OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
# PERFORMANCE OF THIS SOFTWARE.

MESSAGE = "Catopuma is a simple, cross-platform 2D game-creation tool built using SDL2 and Ruby. To create a new game, simply create a Ruby source code file and use the methods described in README.MD to draw stuff, play sounds, and get user input. Greetz to Mecha-Neko, Fizzer, Noelkd, Malouvious and all of the folks at Yozu!"

rgbs = {
  'C' => [255, 0,   0  ],
  'A' => [255, 128, 0  ],
  'T' => [255, 255, 0  ],
  'O' => [0,   255, 0  ],
  'P' => [0,   255, 255],
  'U' => [0,   0,   255],
  'M' => [255, 0,   255]
}

# Load the various images.
font = loadImage "images/04B_30/font.bmp"
logo = loadImage "images/gobusto/logo.png"
alien = loadImage "images/gobusto/alien.bmp"

playMusic "music/chasersgaming/golucky.ogg", -1

x = 320
while checkEvents
  setRGB 32, 64, 128
  clearDisplay

  # Draw the sinewave CATOPUMA text:
  setRGB 0, 0, 0
  fillRect 0, 0, 320, 40

  i = 0
  "CATOPUMA".each_char do |c|
    r, g, b = rgbs[c]
    setRGB r, g, b
    drawText font, 40 + (i*32), 12 + (Math.sin((getTimer / 200) + (i * 32)) * 8).to_i, c
    i += 1
  end

  # Draw the stuff in the middle:
  setRGB 100, 150, 200
  fillRect 0, 40, 320, 200 - 60

  setRGB 255, 255, 255
  drawImage logo, 96 + (Math.cos(getTimer / 200) * 4), 56 + (Math.sin(getTimer / 100) * 2)

  setRGB 255, 0, 0, 127 + Math.sin((getTimer + 123) / 400) * 127
  drawImage alien, 28  + (Math.cos(getTimer / 200) * 16), 74  - (Math.sin(getTimer / 200) * 8), 32

  setRGB 0, 255, 0, 127 + Math.sin((getTimer + 456) / 400) * 127
  drawImage alien, 46 + (Math.sin(getTimer / 200) * 16), 138 + (Math.cos(getTimer / 200) * 8), nil, 16

  setRGB 0, 255, 255, 127 + Math.cos((getTimer + 789) / 400) * 127
  drawImage alien, 270 + (Math.cos(getTimer / 200) * 16), 78 + (Math.sin(getTimer / 200) * 8)

  setRGB 255, 0, 255, 127 + Math.cos((getTimer + 120) / 400) * 127
  drawImage alien, 250 - (Math.sin(getTimer / 200) * 16), 128 + (Math.cos(getTimer / 200) * 8), 32, 16

  # Draw the scrolling text at the bottom of the display:
  x -= 1
  x = 320 if x < 0 - (MESSAGE.length * 16)

  setRGB 0, 0, 0
  fillRect 0, 180, 320, 20
  setRGB 255, 255, 128
  drawText font, x, 200-18, MESSAGE

  # Update the screen / limit the redraw rate to about 100 FPS.
  updateDisplay
  waitFor 10
end
