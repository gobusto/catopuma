#
#  $$  $    $$$$$ $$$$ $   $     $$  $$$$$ $$$$$  $$   $$$  $  $ $$$$ $$$   $$$
# $  $ $      $   $    $$  $    $  $   $     $   $  $ $   $ $ $  $    $  $ $
# $$$$ $      $   $$$$ $ $ $    $$$$   $     $   $$$$ $     $$   $$$$ $$$   $$
# $  $ $      $   $    $  $$    $  $   $     $   $  $ $   $ $ $  $    $  $    $
# $  $ $$$$ $$$$$ $$$$ $   $    $  $   $     $   $  $  $$$  $  $ $$$$ $  $ $$$
#
# Copyright (c) 2016, Thomas Glyn Dennis
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
# REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
# AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
# INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
# LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
# OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
# PERFORMANCE OF THIS SOFTWARE.

JOYPAD = -1  # 0 = First joypad; -1 = Arrow keys + space/return.

$core = 0

class Player
  attr_accessor :x

  def initialize
    self.x = 156
  end

  def update
    self.x += (joyAxis(JOYPAD, 0) * 2).to_i
    self.x = 0 if self.x < 0
    self.x = 320 - 16 if self.x > 320 - 16
  end
end

class Bullet
  attr_accessor :x
  attr_accessor :y

  def initialize(x)
    self.x = x
    self.y = 200 - 24
  end

  def update(aliens)
    aliens.each do |alien|
      if self.x >= alien.x && self.x <= alien.x + 16
        if self.y >= alien.y && self.y <= alien.y + 8
          alien.dead = true
        end
      end
    end
    self.y -= 4
  end
end

class Alien
  attr_accessor :x
  attr_accessor :y
  attr_accessor :r
  attr_accessor :g
  attr_accessor :b
  attr_accessor :dead

  def initialize(x, y, rgb)
    self.x = x
    self.y = y
    self.r = rgb[0]
    self.g = rgb[1]
    self.b = rgb[2]
    self.dead = false
  end

  def update
    if current_direction == :left
      self.x -= 1
      change_direction if self.x <= 0
    else
      self.x += 1
      change_direction if self.x >= 320 - 16
    end
  end

  private

  def current_direction
    @direction ||= :left
  end

  def change_direction
    @direction = (current_direction == :left) ? :right : :left
    self.y += 8
    $core -= 1
    raise "Game Over!" if self.y >= 200 - 16
  end

end

# Load the various graphics/sound effects we're going to use:
font = loadImage "images/04B_30/font.bmp"
player_sprite = loadImage "images/gobusto/player.bmp"
alien_sprite = loadImage "images/gobusto/alien.bmp"
shoot_sound = loadSound "sounds/krial/siclone_shoot_enemy.wav"
hit_sound = loadSound "sounds/krial/siclone_shoot.wav"
playMusic "music/jensan/retro.ogg", -1

# Create some aliens:
aliens = {0 => [255, 0, 0], 1 => [255, 255, 0], 2 => [0, 255, 0]}.map do |y, rgb|
  12.times.map { |x| Alien.new(x * 20, (y * 16) + 8, rgb) }
end.flatten

# Create a player, but don't create a bullet just yet...
player = Player.new
bullet = nil

# IMPORTANT: We need to call checkEvents regularly to handle various things!
while checkEvents

  player.update
  aliens.each(&:update)

  if bullet
    bullet.update aliens
    # If any aliens were hit, play a sound and increase the score.
    playSound hit_sound if aliens.any?(&:dead)
    $core += 100 * aliens.select(&:dead).length
    # Clear out any dead aliens (and destroy the bullet, if necessary).
    bullet = nil if bullet.y <= 0 || aliens.any?(&:dead)
    aliens.reject!(&:dead)
  elsif joyButton(JOYPAD, 0)
    bullet = Bullet.new(player.x + 7)
    playSound shoot_sound
  end

  # Draw everything:
  setRGB 0, 0, 255
  clearDisplay

  aliens.each do |alien|
    setRGB alien.r, alien.g, alien.b
    drawImage alien_sprite, alien.x, alien.y
  end

  setRGB 255, 255, 255
  drawLine bullet.x, bullet.y, 0, 2 if bullet
  drawImage player_sprite, player.x, 200 - 16

  setRGB 0, 255, 255
  drawText(font, 16, 0, "Score: #{$core}")

  updateDisplay
  waitFor 16

  # In lieu of a proper "game over" screen:
  raise "You win!" if aliens.empty?
end
