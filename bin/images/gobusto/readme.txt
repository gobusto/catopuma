ADDRESS: https://gitlab.com/gobusto/catopuma
AUTHORS: gobusto
LICENCE: CC-0 (Public Domain)

These files were created for the examples that come bundled with catopuma.
