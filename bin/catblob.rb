# C A T B L O B   S C E N E
#
# Copyright (c) 2016, Thomas Glyn Dennis
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
# REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
# AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
# INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
# LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
# OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
# PERFORMANCE OF THIS SOFTWARE.

# Load the various images:
font = loadImage("images/gobusto/tinyfont.bmp")

player = [
  loadImage("images/gobusto/catblob1.bmp"),
  loadImage("images/gobusto/catblob2.bmp")
]

tileset = {
  floor: loadImage("images/chasersgaming/greenlands_floor.bmp"),
  solid: loadImage("images/chasersgaming/greenlands_solid.bmp"),
  ledge: loadImage("images/chasersgaming/greenlands_topleft.bmp"),
  redge: loadImage("images/chasersgaming/greenlands_topright.bmp"),
  left:  loadImage("images/chasersgaming/greenlands_left.bmp"),
  right: loadImage("images/chasersgaming/greenlands_right.bmp"),
  sky1:  loadImage("images/igor_gundarev/cloud1.bmp"),
  sky2:  loadImage("images/igor_gundarev/cloud2.bmp"),
  sky3:  loadImage("images/igor_gundarev/cloud3.bmp")
}

# Define a screen of tiles:
world = [
  [:redge, nil,    nil,    nil, nil,    nil,    nil,    nil,    nil,    nil   ],
  [:right, nil,    :sky2,  nil, nil,    nil,    nil,    nil,    nil,    :ledge],
  [:right, nil,    nil,    nil, :sky1,  nil,    nil,    :sky3,  nil,    :left ],
  [:right, nil,    nil,    nil, nil,    nil,    nil,    nil,    :ledge, :solid],
  [:right, nil,    nil,    nil, nil,    nil,    nil,    nil,    :left,  :solid],
  [:solid, :floor, :redge, nil, :ledge, :floor, :floor, :floor, :solid, :solid]
]

while checkEvents
  setRGB 0, 0, 0
  clearDisplay

  # Fill the area behind the tiles with a "sky" colour:
  setRGB 64, 128, 255
  fillRect 0, 0, 320, 32*6

  # Draw the tiles:
  setRGB 255, 255, 255
  world.each_with_index do |row, y|
    row.each_with_index do |tile_id, x|
      drawImage(tileset[tile_id], x * 32, y * 32) if tileset[tile_id]
    end
  end

  # Draw the player:
  drawImage(player[(getTimer / 300) % 2], 160, 140)

  # Draw the status bar:
  setRGB 48, 48, 48
  fillRect 0, 192, 320, 8
  setRGB 255, 255, 0
  drawText font, 0, 192, " -Welcome to catblob's secret hideout!- "

  updateDisplay
  waitFor 20
end
