# Skeleton game
#
# Copyright (c) 2016, Thomas Glyn Dennis
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
# REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
# AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
# INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
# LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
# OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
# PERFORMANCE OF THIS SOFTWARE.

require "./code/actor.rb"
require "./code/item.rb"
require "./code/world.rb"

VID_OFFS_X = 32
VID_OFFS_Y = 48

font = loadImage("images/gobusto/tinyfont.bmp")

border = loadImage("images/gobusto/border.png")

tileset = {
  block: loadImage("images/gobusto/block.bmp"),
  bones: loadImage("images/gobusto/bones.bmp"),
  brick: loadImage("images/gobusto/brick.bmp"),
  grass: loadImage("images/gobusto/grass.bmp"),
  rack1: loadImage("images/gobusto/rack1.bmp"),
  rack2: loadImage("images/gobusto/rack2.bmp"),
  earth: loadImage("images/gobusto/earth.bmp")
}

item_image = {
  key: loadImage("images/gobusto/key.bmp")
}

player_idle = [
  loadImage("images/gobusto/skeledude1.bmp")
]

player_walk = [
  loadImage("images/gobusto/skeledude2.bmp"),
  loadImage("images/gobusto/skeledude3.bmp"),
  loadImage("images/gobusto/skeledude2.bmp"),
  loadImage("images/gobusto/skeledude4.bmp")
]

player = Actor.new 72, 80

items = []
items << Item.new(:key, 224, 32)

while checkEvents
  anim_timer = getTimer / 200

  player.update_logic
  player.update_state

  setRGB 0, 0, 0
  clearDisplay
  setRGB 255, 255, 255

  World.screen.each_with_index do |row, y|
    row.each_with_index do |tile_id, x|
      drawImage(tileset[tile_id], VID_OFFS_X + x * 16, VID_OFFS_Y + y * 16) if tileset[tile_id]
    end
  end

  # Draw the items.
  items.each do |item|
    drawImage item_image[item.kind], VID_OFFS_X + item.x, VID_OFFS_Y + item.y
  end

  # Draw the player
  frame_list = player.idle? ? player_idle : player_walk
  image = frame_list[anim_timer % frame_list.length]
  if player.left?
    drawImage image, VID_OFFS_X + player.x + player.w, VID_OFFS_Y + player.y, -player.w
  else
    drawImage image, VID_OFFS_X + player.x, VID_OFFS_Y + player.y
  end

  # Draw the HUD
  drawImage border, 0, 0
  setRGB 0, 255, 0
  drawText font, 160 - (World.title.length * 4), 184, World.title
  item_text = "Not carrying anything"
  setRGB 255, 0, 0
  drawText font, 160 - (item_text.length * 4), 24, item_text

  updateDisplay
  waitFor 20
end
