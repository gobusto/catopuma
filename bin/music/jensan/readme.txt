ADDRESS: http://opengameart.org/content/retro
AUTHORS: Jensan
LICENCE: CC-0 (Public Domain)

Just a short, slightly annoying, loopable piece that I guess could work as a main menu song.

Or cut it up and remix it till your ears bleed!

Length: 0:45.
