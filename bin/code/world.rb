class World

  TILE_W = 16
  TILE_H = 16

  IGNORE = [nil, :bones]

  def self.title
    "The Old Dungeon"
  end

  def self.screen
  [
    [:brick, :xxxxx, :xxxxx, :xxxxx, :xxxxx, :grass, :grass, :grass, :grass, :grass, :grass, :grass, :grass, :grass, :grass, :grass],
    [:brick, :brick, nil,    nil,    nil,    nil,    nil,    nil,    nil,    nil,    nil,    nil,    nil,    nil,    nil,    :earth],
    [:brick, :brick, :brick, nil,    nil,    nil,    nil,    nil,    nil,    nil,    nil,    nil,    nil,    nil,    nil,    :earth],
    [:block, :block, :block, :block, :block, :block, :block, :block, :block, :block, nil,    nil,    nil,    nil,    :earth, :earth],
    [:block, :bones, nil,    nil,    nil,    nil,    :bones, nil,    nil,    :block, :block, nil,    nil,    nil,    nil,    :earth],
    [:block, nil,    nil,    nil,    nil,    nil,    nil,    nil,    nil,    nil,    :block, :block, nil,    nil,    nil,    :earth],
    [:block, nil,    nil,    nil,    nil,    nil,    nil,    nil,    nil,    nil,    nil,    nil,    nil,    nil,    :earth, :earth],
    [:block, nil,    nil,    nil,    :rack1, :rack2, nil,    nil,    nil,    nil,    nil,    nil,    nil,    :earth, :earth, nil   ],
    [:block, :block, :block, :block, :block, :block, :block, :block, :block, :block, :block, :block, :earth, :earth, nil,    nil   ]
  ]
  end

  def self.tile_at(x, y)
    x = (x / TILE_W).to_i
    y = (y / TILE_H).to_i

    if y >= 0 && y < screen.length && x >= 0 && x < screen[y].length
      kind = self.screen[y][x]
    else
      kind = nil
    end

    !IGNORE.include?(kind) ? { x: x * TILE_W, y: y * TILE_H, kind: kind } : nil
  end

end
