class Item

  def initialize(kind, x, y)
    @kind = kind
    @x = x
    @y = y
  end

  def kind
    @kind
  end

  def x
    @x
  end

  def y
    @y
  end

end
