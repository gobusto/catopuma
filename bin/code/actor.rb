# The "Actor" class used by the skeleton game.
#
# Copyright (c) 2016, Thomas Glyn Dennis
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
# REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
# AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
# INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
# LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
# OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
# PERFORMANCE OF THIS SOFTWARE.

class Actor

  JUMP_SOUND = loadSound "sounds/krial/siclone_menu.wav"

  def initialize(x, y)
    @x = x
    @y = y
    @dx = 0
    @dy = 0
    @on_floor = false
  end

  def update_logic
    @dx = 0
    (-1..0).each do |i|
      @dx = -1 if joyAxis(i, 0) < 0
      @dx = +1 if joyAxis(i, 0) > 0
      if joyButton(i, 0) && @on_floor
        @on_floor = false
        @dy = -4.3
        playSound JUMP_SOUND
      end
    end
  end

  def update_state
    @dy += 0.2 unless @dy >= 5

    if test_collide(@dx, 0)
      @dx = 0
    else
      @x += @dx.to_i
    end

    collision = test_collide(0, @dy)
    if collision
      if @dy < 0
        @y = collision[:y] + World::TILE_H
        @on_floor = false
      else
        @y = collision[:y] - h
        @on_floor = true
      end
      @dy = 0
    else
      @y += @dy.to_i
      @on_floor = false
    end

  end

  def x
    @x
  end

  def y
    @y
  end

  def w
    16
  end

  def h
    16
  end

  def idle?
    @dx == 0
  end

  def left?
    @dx < 0
  end

  def right?
    @dx > 0
  end

  private

  def test_collide(dx, dy)
    World.tile_at(x+dx, y+dy    ) || World.tile_at(x+dx+w-1, y+dy    ) ||
    World.tile_at(x+dx, y+dy+h-1) || World.tile_at(x+dx+w-1, y+dy+h-1)
  end

end
