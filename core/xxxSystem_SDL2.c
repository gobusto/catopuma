/**
@file
@brief An SDL2 implementation of the functions outlined in `xxxSystem.h`

Copyright (C) 2016 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <SDL2/SDL.h>
#include <stdlib.h>
#include <stdio.h>
#include "xxxAudio.h"
#include "xxxGraphics.h"
#include "xxxScript.h"
#include "xxxSystem.h"

/* Global objects and variables. */

SDL_Window   *sys_window = NULL;  /**< @brief The SDL window.         */
SDL_Joystick *sys_joypad = NULL;  /**< @brief The joypad, if present. */

/*
[PUBLIC] Initialise everything.
*/

int sysInit(int argc, char **argv)
{
  SDL_Surface *icon;

  sysQuit();

  if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
  {
    fprintf(stderr, "[ERROR] Could not initialise SDL!\n");
    return -1;
  }

  /* Note that this is a multiple of the "virtual" display resolution below: */
  sys_window = SDL_CreateWindow("Catopuma",
      SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 320*3, 200*3,
      SDL_WINDOW_RESIZABLE);
  if (!sys_window)
  {
    SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "SDL Error",
        "Could not create an SDL window", NULL);
    return -2;
  }

  /* Why 320x200? Because the CPC 464, Atari St, and Amiga 500 use it! :) */
  if (gfxInit(320, 200, sys_window) != 0)
  {
    SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Graphics Error",
        "Could not initialise the graphics system", NULL);
    return -3;
  }

  /* Initialise Python, Ruby, Javascript, or whatever else we're using... */
  if (scriptInit(argc, argv) != 0)
  {
    SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "VM Error",
        "Could not initialise the scripting system", NULL);
    return -4;
  }

  /* Some machines may not have audio hardware, so it's OK if this fails. */
  if (sndInit() != 0) { fprintf(stderr, "[WARNING] Audio init failed...\n"); }

  /* TODO: Support more than one joypad. */
  sys_joypad = SDL_JoystickOpen(0);

  /* Attempt to set a window icon: */
  icon = SDL_LoadBMP("icon.bmp");
  if (icon)
  {
    SDL_SetWindowIcon(sys_window, icon);
    SDL_FreeSurface(icon);
  }

  return 0;
}

/*
[PUBLIC] Shut down everything and free any currently allocated memory.
*/

void sysQuit(void)
{
  if (!SDL_WasInit(0)) { return; }

  SDL_JoystickClose(sys_joypad);
  sys_joypad = NULL;

  sndQuit();
  scriptQuit();
  gfxQuit();

  SDL_DestroyWindow(sys_window);
  sys_window = NULL;

  SDL_Quit();
}

/*
[PUBLIC] This function handles the main progam loop.
*/

void sysRun(const char *file_name)
{
  const char *error;

  if (file_name)
  {
    char new_title[11 + 64 + 1];
    sprintf(new_title, "Catopuma - %.64s", file_name);
    SDL_SetWindowTitle(sys_window, new_title);
  }

  error = scriptLoadFile(file_name);
  if (error)
  {
    SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Error", error, sys_window);
  }
}

/*
[PUBLIC] Handle any system events, such as window moving/resizing.
*/

int sysCheckEvents(void)
{
  SDL_Event msg;

  while (SDL_PollEvent(&msg))
  {
    if (msg.type == SDL_QUIT) { return 0; }
  }

  return 1;
}

/*
[PUBLIC] Get the current program time in milliseconds.
*/

unsigned long sysGetTimer(void) { return (unsigned long)SDL_GetTicks(); }

/*
[PUBLIC] Pause the program for the specified number of milliseconds.
*/

void sysWaitFor(unsigned long ms)
{
  SDL_Delay((Uint32)ms);
}

/*
[PUBLIC] Get the state of a joypad axis.
*/

float sysJoyAxis(int device_id, int idx)
{
  if (device_id < 0)
  {
    const Uint8 *key = SDL_GetKeyboardState(NULL);
    switch (idx)
    {
      case 0: return (float)key[SDL_SCANCODE_RIGHT] - key[SDL_SCANCODE_LEFT];
      case 1: return (float)key[SDL_SCANCODE_DOWN] - key[SDL_SCANCODE_UP];
      default: return 0;
    }
  }
  else if (sys_joypad)
  {
    float value = (SDL_JoystickGetAxis(sys_joypad, idx) + 0.5) / 32767.5;
    return value > -0.2 && value < +0.2 ? 0 : value;
  }
  return 0;
}

/*
[PUBLIC] Get the state of a joypad button.
*/

int sysJoyButton(int device_id, int idx)
{
  if (device_id < 0)
  {
    const Uint8 *key = SDL_GetKeyboardState(NULL);
    switch (idx)
    {
      case 0: return key[SDL_SCANCODE_SPACE];
      case 1: return key[SDL_SCANCODE_RETURN];
      default: return 0;
    }
  }
  else if (sys_joypad) { return SDL_JoystickGetButton(sys_joypad, idx); }
  return 0;
}
