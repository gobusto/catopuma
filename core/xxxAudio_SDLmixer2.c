/**
@file
@brief An SDL2 implementation of the functions outlined in `xxxAudio.h`

Copyright (C) 2016 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <SDL2/SDL_mixer.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "xxxAudio.h"

/* Global objects and variables. */

SDL_bool snd_initialised = SDL_FALSE; /**< @brief Sound initialisation flag. */

Mix_Music *snd_music = NULL; /**< @brief Stores any currently-playing music. */

size_t num_sounds = 0;        /**< @brief The number of sounds loaded. */
Mix_Chunk **snd_sound = NULL; /**< @brief Contains all loaded sounds   */

/*
[PUBLIC] Initialise the audio subsystem.
*/

int sndInit(void)
{
  sndQuit();

  if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) != 0) { return -1; }

  snd_initialised = SDL_TRUE;
  return 0;
}

/*
[PUBLIC] Shut down the audio subsystem.
*/

void sndQuit(void)
{
  if (!snd_initialised) { return; }

  sndStopMusic();

  Mix_HaltChannel(-1);
  while (num_sounds) { Mix_FreeChunk(snd_sound[--num_sounds]); }
  free(snd_sound);
  snd_sound = NULL;

  Mix_CloseAudio();
  snd_initialised = SDL_FALSE;
}

/*
[PUBLIC] Load a sound effect from a file.
*/

int sndLoadSound(const char *file_name)
{
  void *tmp;

  if (!file_name) { return -1; }

  tmp = realloc(snd_sound, sizeof(Mix_Chunk*) * (num_sounds + 1));
  if (!tmp) { return -2; }
  snd_sound = (Mix_Chunk**)tmp;

  snd_sound[num_sounds] = Mix_LoadWAV(file_name);
  if (!snd_sound[num_sounds]) { return -3; }

  return num_sounds++;
}

/*
[PUBLIC] Play a sound effect.
*/

void sndPlaySound(int idx)
{
  if (idx < 0 || (size_t)idx >= num_sounds) { return; }
  Mix_PlayChannel(-1, snd_sound[idx], 0);
}

/*
[PUBLIC] Play (and loop) a music track.
*/

int sndPlayMusic(const char *file_name, int num_loops)
{
  if (!snd_initialised) { return 1; }

  sndStopMusic();

  if (!file_name) { return -1; }
  snd_music = Mix_LoadMUS(file_name);
  if (!snd_music) { return -2; }
  if (Mix_PlayMusic(snd_music, num_loops) != 0) { return -3; }

  return 0;
}

/*
[PUBLIC] Stop any currently playing music.
*/

void sndStopMusic(void)
{
  if (!snd_initialised) { return; }

  Mix_HaltMusic();
  Mix_FreeMusic(snd_music);
  snd_music = NULL;
}

/*
[PUBLIC] Returns non-zero if music is currently playing.
*/

int sndMusicPlaying(void) { return snd_music != NULL; }
