/**
@file
@brief Handles basic GUI stuff such a user input and window events.

Copyright (C) 2016 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef __XXX_SYSTEM_H__
#define __XXX_SYSTEM_H__

#ifdef __cplusplus
extern "C" {
#endif

/**
@brief Initialise everything.

This function should be called when the program first starts.

@param argc Length of the argv array.
@param argv Array of progam arguments.
@return Zero on success, or non-zero on failure.
*/

int sysInit(int argc, char **argv);

/**
@brief Shut down everything and free any currently allocated memory.

This function should be called when the program terminates.
*/

void sysQuit(void);

/**
@brief This function handles the main progam loop.

Once called, this function will continue to run until a termination event is
received.

@param file_name The name of the file to run.
*/

void sysRun(const char *file_name);

/**
@brief Handle any system events, such as window moving/resizing.

This should be called at regular intervals to prevent a backlog of unhandled
events.

@return `false` if the user has tried to quit the program since it last ran, or
`true` if not.
*/

int sysCheckEvents(void);

/**
@brief Get the current program time in milliseconds.

@return The current program time in milliseconds.
*/

unsigned long sysGetTimer(void);

/**
@brief Pause the program for the specified number of milliseconds.

Be careful - system events aren't handled during the delay, so user-quit events
will be ignored until `sysCheckEvents()` is called.

@param ms The length of the delay.
*/

void sysWaitFor(unsigned long ms);

/**
@brief Get the state of a joypad axis.

The first *real* joypad uses index `0`, but you can also use a *negative* index
to check the arrow keys instead.

Axes range from `-1.0` to `+1.0`, and start at index `0`. Axis `0` is (usually)
left/right on the "main" D-pad or analog stick, with axis `1` used for up/down.
The other axes will vary depending on the type of joypad used.

@param device_id The ID of the joypad to check.
@param idx The ID of the axis to check.
@return A value ranging from `-1.0` to `+1.0`.
*/

float sysJoyAxis(int device_id, int idx);

/**
@brief Get the state of a joypad button.

The first *real* joypad uses index `0`, but you can also use a *negative* index
to check the space/return keys instead.

@param device_id The ID of the joypad to check.
@param idx The ID of the button to check.
@return `true` if the button is currently pressed, or `false` if not.
*/

int sysJoyButton(int device_id, int idx);

#ifdef __cplusplus
}
#endif

#endif /* __XXX_SYSTEM_H__ */
