/**
@file
@brief A language-angnostic interface to the scripting logic.

Copyright (C) 2016 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef __XXX_SCRIPT_H__
#define __XXX_SCRIPT_H__

#ifdef __cplusplus
extern "C" {
#endif

/**
@brief Initialise the VM.

@param argc Length of the argv array.
@param argv Array of progam arguments.
@return Zero on success, or non-zero on failure.
*/

int scriptInit(int argc, char **argv);

/**
@brief Shut down the VM.
*/

void scriptQuit(void);

/**
@brief Load and run a script from a file.

@param file_name The path of the file to be loaded.
@return NULL on success, or a pointer to an error string if something fails.
*/

const char *scriptLoadFile(const char *file_name);

#ifdef __cplusplus
}
#endif

#endif /* __XXX_SCRIPT_H__ */
