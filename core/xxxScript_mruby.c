/**
@file
@brief An mruby implementation of the functions defined in `xxxScript.h`

Copyright (C) 2016 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <mruby.h>
#include <mruby/compile.h>
#include <mruby/string.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "xxxAudio.h"
#include "xxxGraphics.h"
#include "xxxScript.h"
#include "xxxSystem.h"

/* Global objects and variables. */

mrb_state    *script_vm  = NULL;  /**< @brief The VM used to run Ruby code. */
mrbc_context *script_ctx = NULL;  /**< @brief Context (for exception info). */

/**
@brief This macro silences "unused parameter" warnings in API functions:
*/

#define SCRIPT_IGNORE_PARAMS if (mrb || (unsigned long)&self > 123) { }

/**
@brief MRuby doesn't support "require", so emulate it as an API function.

Unlike the "real" require, we simply return `false` if the file failed to load.

@param mrb The VM that has called the method (always `script_vm` in this case).
@param self The object that this method was called for (always `Object` here).
@return `true` if the file was loaded, or `false` if something went wrong.
*/

static mrb_value API_require(mrb_state *mrb, mrb_value self)
{
  const char *s;
  mrb_value o;

  SCRIPT_IGNORE_PARAMS

  mrb_get_args(mrb, "o", &o);
  s = mrb_string_value_ptr(mrb, mrb_obj_as_string(mrb, o));

  return scriptLoadFile(s) ? mrb_false_value() : mrb_true_value();
}

/**
@brief Handle any pending system events.

@param mrb The VM that has called the method (always `script_vm` in this case).
@param self The object that this method was called for (always `Object` here).
@return `false` if the user tried to quit, or `true` if not.
*/

static mrb_value API_checkEvents(mrb_state *mrb, mrb_value self)
{
  SCRIPT_IGNORE_PARAMS
  return sysCheckEvents() ? mrb_true_value() : mrb_false_value();
}

/**
@brief Get the current program time in milliseconds.

@param mrb The VM that has called the method (always `script_vm` in this case).
@param self The object that this method was called for (always `Object` here).
@return The current time in milliseconds as a Ruby fixnum.
*/

static mrb_value API_getTimer(mrb_state *mrb, mrb_value self)
{
  SCRIPT_IGNORE_PARAMS
  return mrb_fixnum_value((mrb_int)sysGetTimer());
}

/**
@brief Wait for a specified number of milliseconds.

@param mrb The VM that has called the method (always `script_vm` in this case).
@param self The object that this method was called for (always `Object` here).
@return Always returns `nil`.
*/

static mrb_value API_waitFor(mrb_state *mrb, mrb_value self)
{
  mrb_int i;

  SCRIPT_IGNORE_PARAMS

  mrb_get_args(mrb, "i", &i);
  sysWaitFor((unsigned long)i);
  return mrb_nil_value();
}

/**
@brief Get the state of a joypad axis.

@param mrb The VM that has called the method (always `script_vm` in this case).
@param self The object that this method was called for (always `Object` here).
@return The value of the axis as a -1.0 to + 1.0 floating point value.
*/

static mrb_value API_joyAxis(mrb_state *mrb, mrb_value self)
{
  mrb_int device_id, idx;

  SCRIPT_IGNORE_PARAMS

  mrb_get_args(mrb, "ii", &device_id, &idx);
  return mrb_float_value(mrb, sysJoyAxis((int)device_id, (int)idx));
}

/**
@brief Get the state of a joypad button.

@param mrb The VM that has called the method (always `script_vm` in this case).
@param self The object that this method was called for (always `Object` here).
@return `true` if the key is pressed, or `false` if not.
*/

static mrb_value API_joyButton(mrb_state *mrb, mrb_value self)
{
  mrb_int device_id, idx;

  SCRIPT_IGNORE_PARAMS

  mrb_get_args(mrb, "ii", &device_id, &idx);
  return sysJoyButton((int)device_id, (int)idx) ? mrb_true_value() : mrb_false_value();
}

/**
@brief Clear the display.

@param mrb The VM that has called the method (always `script_vm` in this case).
@param self The object that this method was called for (always `Object` here).
@return Always returns `nil`.
*/

static mrb_value API_clearDisplay(mrb_state *mrb, mrb_value self)
{
  SCRIPT_IGNORE_PARAMS
  gfxClearDisplay();
  return mrb_nil_value();
}

/**
@brief Update the display.

@param mrb The VM that has called the method (always `script_vm` in this case).
@param self The object that this method was called for (always `Object` here).
@return Always returns `nil`.
*/

static mrb_value API_updateDisplay(mrb_state *mrb, mrb_value self)
{
  SCRIPT_IGNORE_PARAMS
  gfxUpdateDisplay();
  return mrb_nil_value();
}

/**
@brief Set the current drawing colour.

@param mrb The VM that has called the method (always `script_vm` in this case).
@param self The object that this method was called for (always `Object` here).
@return Always returns `nil`.
*/

static mrb_value API_setRGB(mrb_state *mrb, mrb_value self)
{
  unsigned char a_chr;
  const unsigned char *a_ptr = NULL;

  mrb_int num_args, r, g, b, a;

  SCRIPT_IGNORE_PARAMS

  num_args = mrb_get_args(mrb, "iii|i", &r, &g, &b, &a);

  if (num_args >= 4)
  {
    a_chr = (unsigned char)a;
    a_ptr = &a_chr;
  }

  gfxSetRGB((int)r, (int)g, (int)b, a_ptr);
  return mrb_nil_value();
}

/**
@brief Draw a line.

@param mrb The VM that has called the method (always `script_vm` in this case).
@param self The object that this method was called for (always `Object` here).
@return Always returns `nil`.
*/

static mrb_value API_drawLine(mrb_state *mrb, mrb_value self)
{
  mrb_int x, y, w, h;

  SCRIPT_IGNORE_PARAMS

  mrb_get_args(mrb, "iiii", &x, &y, &w, &h);
  gfxDrawLine((int)x, (int)y, (int)w, (int)h);
  return mrb_nil_value();
}

/**
@brief Draw a rectangle.

@param mrb The VM that has called the method (always `script_vm` in this case).
@param self The object that this method was called for (always `Object` here).
@return Always returns `nil`.
*/

static mrb_value API_drawRect(mrb_state *mrb, mrb_value self)
{
  mrb_int x, y, w, h;

  SCRIPT_IGNORE_PARAMS

  mrb_get_args(mrb, "iiii", &x, &y, &w, &h);
  gfxDrawRect((int)x, (int)y, (int)w, (int)h);
  return mrb_nil_value();
}

/**
@brief Draw a filled rectangle.

@param mrb The VM that has called the method (always `script_vm` in this case).
@param self The object that this method was called for (always `Object` here).
@return Always returns `nil`.
*/

static mrb_value API_fillRect(mrb_state *mrb, mrb_value self)
{
  mrb_int x, y, w, h;

  SCRIPT_IGNORE_PARAMS

  mrb_get_args(mrb, "iiii", &x, &y, &w, &h);
  gfxFillRect((int)x, (int)y, (int)w, (int)h);
  return mrb_nil_value();
}

/**
@brief Load an image.

@param mrb The VM that has called the method (always `script_vm` in this case).
@param self The object that this method was called for (always `Object` here).
@return The ID of the image on success, or `nil` on failure.
*/

static mrb_value API_loadImage(mrb_state *mrb, mrb_value self)
{
  const char *s;
  mrb_value o;
  int result;

  SCRIPT_IGNORE_PARAMS

  mrb_get_args(mrb, "o", &o);
  s = mrb_string_value_ptr(mrb, mrb_obj_as_string(mrb, o));

  result = gfxLoadImage(s);
  return result < 0 ? mrb_nil_value() : mrb_fixnum_value((mrb_int)result);
}

/**
@brief Draw an image, with the top-left corner located at `x, y`.

@param mrb The VM that has called the method (always `script_vm` in this case).
@param self The object that this method was called for (always `Object` here).
@return Always returns `nil`.
*/

static mrb_value API_drawImage(mrb_state *mrb, mrb_value self)
{
  int w_int, h_int;
  const int *w_ptr = NULL;
  const int *h_ptr = NULL;

  mrb_int num_args, i, x, y;
  mrb_value w, h;

  SCRIPT_IGNORE_PARAMS

  num_args = mrb_get_args(mrb, "iii|o|o", &i, &x, &y, &w, &h);

  /* The "width" parameter may be (A) missing or (B) ignored (i.e. `nil`). */
  if (num_args >= 4 && mrb_fixnum_p(w))
  {
    w_int = (int)mrb_fixnum(w);
    w_ptr = &w_int;
  }

  /* The "height" parameter may be (A) missing or (B) ignored (i.e. `nil`). */
  if (num_args >= 5 && mrb_fixnum_p(h))
  {
    h_int = (int)mrb_fixnum(h);
    h_ptr = &h_int;
  }

  gfxDrawImage((int)i, (int)x, (int)y, w_ptr, h_ptr);
  return mrb_nil_value();
}

/**
@brief Draw a string of text, with the top-left corner located at `x, y`.

@param mrb The VM that has called the method (always `script_vm` in this case).
@param self The object that this method was called for (always `Object` here).
@return Always returns `nil`.
*/

static mrb_value API_drawText(mrb_state *mrb, mrb_value self)
{
  const char *s;
  mrb_value o;
  mrb_int i, x, y;

  SCRIPT_IGNORE_PARAMS

  mrb_get_args(mrb, "iiio", &i, &x, &y, &o);
  s = mrb_string_value_ptr(mrb, mrb_obj_as_string(mrb, o));

  gfxDrawText((int)i, (int)x, (int)y, s);
  return mrb_nil_value();
}

/**
@brief Load a sound.

@param mrb The VM that has called the method (always `script_vm` in this case).
@param self The object that this method was called for (always `Object` here).
@return The ID of the sound on success, or `nil` on failure.
*/

static mrb_value API_loadSound(mrb_state *mrb, mrb_value self)
{
  const char *s;
  mrb_value o;
  int result;

  SCRIPT_IGNORE_PARAMS

  mrb_get_args(mrb, "o", &o);
  s = mrb_string_value_ptr(mrb, mrb_obj_as_string(mrb, o));

  result = sndLoadSound(s);
  return result < 0 ? mrb_nil_value() : mrb_fixnum_value((mrb_int)result);
}

/**
@brief Play the specified a sound.

@param mrb The VM that has called the method (always `script_vm` in this case).
@param self The object that this method was called for (always `Object` here).
@return Always returns `nil`.
*/

static mrb_value API_playSound(mrb_state *mrb, mrb_value self)
{
  mrb_int i;

  SCRIPT_IGNORE_PARAMS

  mrb_get_args(mrb, "i", &i);

  sndPlaySound((int)i);
  return mrb_nil_value();
}

/**
@brief Load and play a music file.

@param mrb The VM that has called the method (always `script_vm` in this case).
@param self The object that this method was called for (always `Object` here).
@return `true` if the music could be loaded and played, or `false` if not.
*/

static mrb_value API_playMusic(mrb_state *mrb, mrb_value self)
{
  const char *s;
  mrb_value o;
  mrb_int i;

  SCRIPT_IGNORE_PARAMS

  mrb_get_args(mrb, "oi", &o, &i);
  s = mrb_string_value_ptr(mrb, mrb_obj_as_string(mrb, o));

  return sndPlayMusic(s, (int)i) == 0 ? mrb_true_value() : mrb_false_value();
}

/**
@brief Stop any currently playing music.

@param mrb The VM that has called the method (always `script_vm` in this case).
@param self The object that this method was called for (always `Object` here).
@return Always returns `nil`.
*/

static mrb_value API_stopMusic(mrb_state *mrb, mrb_value self)
{
  SCRIPT_IGNORE_PARAMS
  sndStopMusic();
  return mrb_nil_value();
}

/**
@brief Determine if there is currently any music playing.

@param mrb The VM that has called the method (always `script_vm` in this case).
@param self The object that this method was called for (always `Object` here).
@return `true` if there is currently any music playing, or `false` if not.
*/

static mrb_value API_musicPlaying(mrb_state *mrb, mrb_value self)
{
  SCRIPT_IGNORE_PARAMS
  return sndMusicPlaying() ? mrb_true_value() : mrb_false_value();
}

/*
[PUBLIC] Initialise the VM.
*/

int scriptInit(int argc, char **argv)
{
  if (argc || argv) { /* Not currently used for anything... */ }

  scriptQuit();

  script_vm = mrb_open();
  if (!script_vm) { return -1; }
  script_ctx = mrbc_context_new(script_vm);
  if (!script_ctx) { return -2; }
  script_ctx->capture_errors = 1;

  mrb_define_method(script_vm, script_vm->object_class, "require", API_require, MRB_ARGS_NONE());

  mrb_define_method(script_vm, script_vm->object_class, "checkEvents", API_checkEvents, MRB_ARGS_NONE());

  mrb_define_method(script_vm, script_vm->object_class, "getTimer", API_getTimer, MRB_ARGS_NONE());
  mrb_define_method(script_vm, script_vm->object_class, "waitFor", API_waitFor, MRB_ARGS_NONE());

  mrb_define_method(script_vm, script_vm->object_class, "joyAxis", API_joyAxis, MRB_ARGS_NONE());
  mrb_define_method(script_vm, script_vm->object_class, "joyButton", API_joyButton, MRB_ARGS_NONE());

  mrb_define_method(script_vm, script_vm->object_class, "clearDisplay", API_clearDisplay, MRB_ARGS_NONE());
  mrb_define_method(script_vm, script_vm->object_class, "updateDisplay", API_updateDisplay, MRB_ARGS_NONE());

  mrb_define_method(script_vm, script_vm->object_class, "setRGB", API_setRGB, MRB_ARGS_NONE());
  mrb_define_method(script_vm, script_vm->object_class, "drawLine", API_drawLine, MRB_ARGS_NONE());
  mrb_define_method(script_vm, script_vm->object_class, "drawRect", API_drawRect, MRB_ARGS_NONE());
  mrb_define_method(script_vm, script_vm->object_class, "fillRect", API_fillRect, MRB_ARGS_NONE());

  mrb_define_method(script_vm, script_vm->object_class, "loadImage", API_loadImage, MRB_ARGS_NONE());
  mrb_define_method(script_vm, script_vm->object_class, "drawImage", API_drawImage, MRB_ARGS_NONE());
  mrb_define_method(script_vm, script_vm->object_class, "drawText", API_drawText, MRB_ARGS_NONE());

  mrb_define_method(script_vm, script_vm->object_class, "loadSound", API_loadSound, MRB_ARGS_NONE());
  mrb_define_method(script_vm, script_vm->object_class, "playSound", API_playSound, MRB_ARGS_NONE());

  mrb_define_method(script_vm, script_vm->object_class, "playMusic", API_playMusic, MRB_ARGS_NONE());
  mrb_define_method(script_vm, script_vm->object_class, "stopMusic", API_stopMusic, MRB_ARGS_NONE());
  mrb_define_method(script_vm, script_vm->object_class, "musicPlaying", API_musicPlaying, MRB_ARGS_NONE());

  return 0;
}

/*
[PUBLIC] Shut down the VM.
*/

void scriptQuit(void)
{
  if (script_ctx)
  {
    mrbc_context_free(script_vm, script_ctx);
    script_ctx = NULL;
  }
  if (script_vm)
  {
    mrb_close(script_vm);
    script_vm = NULL;
  }
}

/*
[PUBLIC] Load and run a script from a file.
*/

const char *scriptLoadFile(const char *file_name)
{
  FILE *src;

  if (!script_vm) { return "VM not initialised"; }

  if (!file_name) { return "No file name specified"; }
  src = fopen(file_name, "rb");
  if (!src) { return "Could not open file"; }
  mrbc_filename(script_vm, script_ctx, file_name);
  mrb_load_file_cxt(script_vm, src, script_ctx);
  fclose(src);

  if (script_vm->exc)
  {
    const mrb_value exc = mrb_obj_value(script_vm->exc);
    const char *line = mrb_string_value_ptr(script_vm, mrb_obj_as_string(script_vm, mrb_attr_get(script_vm, exc, mrb_intern_lit(script_vm, "line"))));
    const char *text = mrb_string_value_ptr(script_vm, mrb_obj_as_string(script_vm, mrb_attr_get(script_vm, exc, mrb_intern_lit(script_vm, "mesg"))));
    size_t len = strlen(text);

    /* Syntax exceptions end with `\n` + don't set `line`/`file` attributes: */
    fprintf(stderr, "[EXCEPTION] %s", text);
    if (len > 0 && text[len-1] != '\n') { fputc('\n', stderr); }
    fprintf(stderr, "\t(");
    if (*line) { fprintf(stderr, "Line %s of ", line); }
    fprintf(stderr, "%s)\n", file_name);

    /* Use the actual text for the message box text: */
    return text;
  }

  return NULL;
}
