/**
@file
@brief A language-angnostic interface to the video subsystem.

Copyright (C) 2016 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef __XXX_GRAPHICS_H__
#define __XXX_GRAPHICS_H__

#ifdef __cplusplus
extern "C" {
#endif

/**
@brief Initialise the rendering subsystem.

@param w The width of the canvas in pixels.
@param h The height of the canvas in pixels.
@param data Implementation-specific window data.
@return Zero on success, or non-zero on failure.
*/

int gfxInit(int w, int h, void *data);

/**
@brief Shut down the rendering subsystem.
*/

void gfxQuit(void);

/**
@brief Clear the screen, using the current RGB value.
*/

void gfxClearDisplay(void);

/**
@brief Update the screen.

Call this once you've finished drawing everything.
*/

void gfxUpdateDisplay(void);

/**
@brief Set the current drawing colour.

@param r The new red value.
@param g The new green value.
@param b The new blue value.
@param a The new alpha value; defaults to 255 if not specified.
*/

void gfxSetRGB(unsigned char r, unsigned char g, unsigned char b, const unsigned char *a);

/**
@brief Draw a line from `x, y` to `x+w, y+h` using the current RGB value.

@param x The X-origin or the line.
@param y The Y-origin of the line.
@param w The horizontal offset to the end of the line.
@param h The vertical offset to the end of the line.
*/

void gfxDrawLine(int x, int y, int w, int h);

/**
@brief Draw a rectangle *outline* from `x, y` to `x+w, y+h`.

@param x The X-origin or the rectangle.
@param y The Y-origin of the rectangle.
@param w The width of the rectangle.
@param h The height of the rectangle.
*/

void gfxDrawRect(int x, int y, int w, int h);

/**
@brief Draw a *filled* rectangle from `x, y` to `x+w, y+h`.

@param x The X-origin or the rectangle.
@param y The Y-origin of the rectangle.
@param w The width of the rectangle.
@param h The height of the rectangle.
*/

void gfxFillRect(int x, int y, int w, int h);

/**
@brief Load an image from a file.

@param file_name The name (and path) of the image file to load.
@return A non-negative ID value on success, or a negative value on failure.
*/

int gfxLoadImage(const char *file_name);

/**
@brief Draw an image, with the top-left corner located at `x, y`.

@param idx The ID of the image to draw.
@param x The X position of the left-most edge of the image.
@param y The Y position of the top-most edge of the image.
@param w Optional - if specified, the image is scaled to fit this width.
@param h Optional - if specified, the image is scaled to fit this height.
*/

void gfxDrawImage(int idx, int x, int y, const int *w, const int *h);

/**
@brief Draw a string of text, with the top-left corner located at `x, y`.

This works by dividing the image into a 16x8 grid and mapping ASCII code points
on to it. Non-ASCII characters are mapped to `0` (the "null" character").

@param idx The ID of the image to draw.
@param x The X position of the left-most edge of the image.
@param y The Y position of the top-most edge of the image.
@param text The text to display.
*/

void gfxDrawText(int idx, int x, int y, const char *text);

#ifdef __cplusplus
}
#endif

#endif /* __XXX_GRAPHICS_H__ */
