/**
@file
@brief An SDL2 implementation of the functions outlined in `xxxGraphics.h`

Copyright (C) 2016 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "xxxGraphics.h"

/* Global objects and variables. */

SDL_Renderer *gfx_renderer = NULL;  /**< The SDL renderer. */

size_t num_images = 0;          /**< @brief The number of images loaded. */
SDL_Texture **gfx_image = NULL; /**< @brief Contains all loaded images   */

/*
[PUBLIC] Initialise the rendering subsystem.
*/

int gfxInit(int w, int h, void *data)
{
  gfxQuit();

  if (w < 1 || h < 1 || !data) { return -1; }

  gfx_renderer = SDL_CreateRenderer((SDL_Window*)data, -1, 0);
  if (!gfx_renderer) { return -2; }
  SDL_RenderSetLogicalSize(gfx_renderer, w, h);

  /* Just in case the script file fails to load/draw anything... */
  gfxClearDisplay();
  gfxUpdateDisplay();

  return 0;
}

/*
[PUBLIC] Shut down the rendering subsystem.
*/

void gfxQuit(void)
{
  if (!gfx_renderer) { return; }

  while (num_images) { SDL_DestroyTexture(gfx_image[--num_images]); }
  free(gfx_image);
  gfx_image = NULL;

  SDL_DestroyRenderer(gfx_renderer);
  gfx_renderer = NULL;
}

/*
[PUBLIC] Clear the screen, using the current RGB value.
*/

void gfxClearDisplay(void) { SDL_RenderClear(gfx_renderer); }

/*
[PUBLIC] Update the screen.
*/

void gfxUpdateDisplay(void) { SDL_RenderPresent(gfx_renderer); }

/*
[PUBLIC] Set the current drawing colour.
*/

void gfxSetRGB(unsigned char r, unsigned char g, unsigned char b, const unsigned char *a)
{
  const unsigned char alpha = a ? *a : 255;
  SDL_BlendMode mode = alpha < 255 ? SDL_BLENDMODE_BLEND : SDL_BLENDMODE_NONE;
  size_t i;

  SDL_SetRenderDrawColor(gfx_renderer, r, g, b, alpha);
  SDL_SetRenderDrawBlendMode(gfx_renderer, mode);

  for (i = 0; i < num_images; ++i)
  {
    SDL_SetTextureColorMod(gfx_image[i], r, g, b);
    SDL_SetTextureAlphaMod(gfx_image[i], alpha);
    /*SDL_SetTextureBlendMode(gfx_image[i], mode);*/
  }
}

/*
[PUBLIC] Draw a line from `x, y` to `x+w, y+h` using the current RGB value.
*/

void gfxDrawLine(int x, int y, int w, int h)
{
  SDL_RenderDrawLine(gfx_renderer, x, y, x+w, y+h);
}

/*
[PUBLIC] Draw a rectangle *outline* from `x, y` to `x+w, y+h`.
*/

void gfxDrawRect(int x, int y, int w, int h)
{
  SDL_Rect rect;
  rect.x = x;
  rect.y = y;
  rect.w = w;
  rect.h = h;
  SDL_RenderDrawRect(gfx_renderer, &rect);
}

/*
[PUBLIC] Draw a *filled* rectangle from `x, y` to `x+w, y+h`.
*/

void gfxFillRect(int x, int y, int w, int h)
{
  SDL_Rect rect;
  rect.x = x;
  rect.y = y;
  rect.w = w;
  rect.h = h;
  SDL_RenderFillRect(gfx_renderer, &rect);
}

/*
[PUBLIC] Load an image from a file.
*/

int gfxLoadImage(const char *file_name)
{
  SDL_Surface *surface;
  void *tmp;

  tmp = realloc(gfx_image, sizeof(SDL_Texture*) * (num_images + 1));
  if (!tmp) { return -1; }
  gfx_image = (SDL_Texture**)tmp;

  if (!file_name) { return -1; }
  surface = IMG_Load(file_name);  /* SDL_LoadBMP(file_name); */
  if (!surface) { return -2; }

  gfx_image[num_images] = SDL_CreateTextureFromSurface(gfx_renderer, surface);
  SDL_FreeSurface(surface);
  if (!gfx_image[num_images]) { return -3; }

  return num_images++;
}

/*
[PUBLIC] Draw an image, with the top-left corner located at `x, y`.
*/

void gfxDrawImage(int idx, int x, int y, const int *w, const int *h)
{
  SDL_Rect dst;
  SDL_RendererFlip flip = SDL_FLIP_NONE;

  if (idx < 0 || (size_t)idx >= num_images) { return; }

  SDL_QueryTexture(gfx_image[idx], NULL, NULL, &dst.w, &dst.h);

  dst.x = x;
  dst.y = y;
  if (w) { dst.w = *w; }
  if (h) { dst.h = *h; }

  if (dst.w < 0) { dst.w = -dst.w; dst.x -= dst.w; flip |= SDL_FLIP_HORIZONTAL; }
  if (dst.h < 0) { dst.h = -dst.h; dst.y -= dst.h; flip |= SDL_FLIP_VERTICAL;   }

  SDL_RenderCopyEx(gfx_renderer, gfx_image[idx], NULL, &dst, 0, NULL, flip);
}

/*
[PUBLIC] Draw a string of text, with the top-left corner located at `x, y`.
*/

void gfxDrawText(int idx, int x, int y, const char *text)
{
  SDL_Rect src;
  SDL_Rect dst;
  size_t i;

  if (idx < 0 || (size_t)idx >= num_images || !text) { return; }

  SDL_QueryTexture(gfx_image[idx], NULL, NULL, &src.w, &src.h);
  src.w /= 16;
  src.h /= 8;

  dst.x = x;
  dst.y = y;
  dst.w = src.w;
  dst.h = src.h;

  for (i = 0; text[i]; ++i)
  {
    if ((unsigned char)text[i] < 128)
    {
      src.x = src.w * (text[i] % 16);
      src.y = src.h * (text[i] / 16);
    }
    else
    {
      src.x = 0;
      src.y = 0;
    }
    SDL_RenderCopy(gfx_renderer, gfx_image[idx], &src, &dst);
    dst.x += dst.w;
  }
}
