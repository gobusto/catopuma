/**
@file
@brief A language-angnostic interface to the audio subsystem.

Copyright (C) 2016 Thomas Glyn Dennis.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef __XXX_AUDIO_H__
#define __XXX_AUDIO_H__

#ifdef __cplusplus
extern "C" {
#endif

/**
@brief Initialise the audio subsystem.

@return Zero on success, or non-zero on failure.
*/

int sndInit(void);

/**
@brief Shut down the audio subsystem.
*/

void sndQuit(void);

/**
@brief Load a sound effect from a file.

@param file_name The name (and path) of the sound file to load.
@return A non-negative ID value on success, or a negative value on failure.
*/

int sndLoadSound(const char *file_name);

/**
@brief Play a sound effect.

@param idx The ID of the sound to play.
*/

void sndPlaySound(int idx);

/**
@brief Play (and optionally loop) a music file.

Any currently-playing music is stopped first.

@param file_name The name (and path) of the music file to play.
@param num_loops The number of repetitions. Loops forever if less than zero.
@return Zero on success, or non-zero on failure.
*/

int sndPlayMusic(const char *file_name, int num_loops);

/**
@brief Stop any currently-playing music.
*/

void sndStopMusic(void);

/**
@brief Determine if any music is currently being played.

@return True if music is being currently being played, or false if not.
*/

int sndMusicPlaying(void);

#ifdef __cplusplus
}
#endif

#endif /* __XXX_AUDIO_H__ */
