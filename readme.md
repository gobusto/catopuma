Catopuma - A very simple 2D game-creation tool
==============================================

The program tries to load a file called `main.rb` by default, but only if there
are no command-line arguments:

+ `./catopuma` - The program will attempt to load `main.rb`.
+ `./catopuma hello.rb` - The program will attempt to load `hello.rb` instead.

Hint: GUI-based file explorers will often allow you to drag-and-drop `hello.rb`
on to the Catopuma executable file, which has the same effect.

API
---

The following functions are available to Ruby scripts:

### `checkEvents()`

Handle any system events, such as window moving/resizing. This should be called
at regular intervals to prevent a backlog of unhandled events.

Returns `false` if the user has tried to quit the program since it last ran, or
`true` if not.

### `getTimer()`

Returns the current program time in milliseconds.

### `waitFor(milliseconds)`

Pauses the program for the specified number of milliseconds.

Be careful - system events aren't handled during the delay, so user-quit events
will be ignored until `checkEvents()` is called.

### `joyAxis(joypad_id, axis_id)`

Get the state of a joypad axis.

The first *real* joypad uses index `0`, but you can also use a *negative* index
to check the keyboard instead:

+ Using `joyAxis(-1, 0)` gets the state of the left/right arrow keys.
+ Using `joyAxis(-1, 1)` gets the state of the up/down arrow keys.

Axes range from `-1.0` to `+1.0`, and start at index `0`. Axis `0` is (usually)
left/right on the "main" D-pad or analog stick, with axis `1` used for up/down.
The other axes will vary depending on the type of joypad used.

### `joyButton(joypad_id, button_id)`

Get the state of a joypad button.

The first *real* joypad uses index `0`, but you can also use a *negative* index
to check the keyboard instead:

+ Using `joyButton(-1, 0)` gets the state of the spacebar.
+ Using `joyButton(-1, 1)` gets the state of the return key.

Buttons are reported as `true` if they're currently pressed, or `false` if not.

### `clearDisplay()`

Clears the screen, using the current RGB value.

### `updateDisplay()`

Updates the screen. Call this once you've finished drawing everything.

### `setRGB(red, green, blue{, alpha})`

Set the current drawing colour. Each value should range from `0` to `255`.

The alpha value is assumed to be `255` (fully opaque) if not specified.

### `drawLine(x, y, w, h)`

Draw a line from `x, y` to `x+w, y+h` using the current RGB value.

### `drawRect(x, y, w, h)`

Draw a rectangle outline from `x, y` to `x+w, y+h` using the current RGB value.

### `fillRect(x, y, w, h)`

Draw a filled rectangle from `x, y` to `x+w, y+h` using the current RGB value.

### `loadImage(file_name)`

Load an image. Any format supported by SDL2 Image (BMP, PNG, etc.) will work.

Returns the ID of the image on success, or `nil` on failure.

### `drawImage(image, x, y{, w, h})`

Draw an image, with the top-left corner located at `x, y`, optionally stretched
so that it is a specific width or height:

    # Draw the image without stretching it in any way:
    drawImage image, 0, 0
    drawImage image, 0, 0, nil
    drawImage image, 0, 0, nil, nil

    # Stretch the image horizontally, but NOT vertically:
    drawImage image, 0, 0, 123
    drawImage image, 0, 0, 123, nil

    # Stretch the image vertically, but NOT horizontally:
    drawImage image, 0, 0, nil, 123

    # Stretch the image both horizontally AND vertically:
    drawImage image, 0, 0, 123, 123

### `drawText(image, x, y, text)`

Draw a string of text, with the top-left corner located at `x, y`.

This works by dividing the image into a 16x8 grid and mapping ASCII code points
on to it. Non-ASCII characters are mapped to `0` (the "null" character").

### `loadSound(file_name)`

Load a sound. Any format supported by SDL2 Mixer (WAV, OGG, etc.) will work.

Returns the ID of the sound on success, or `nil` on failure.

### `playSound(sound_id)`

Play the specified a sound.

### `playMusic(file_name, loop_count)`

Load and play a music file. Any format supported by SDL2 Mixer will work.

If `loop_count` is `0`, the music will play once. If it is `-1`, the music will
loop forever (or until `stopMusic()` is called).

Returns `true` if the music could be loaded and played, or `false` if not.

### `stopMusic()`

Stop any currently playing music.

### `musicPlaying()`

Returns `true` if there is currently any music playing, or `false` if not.

FAQ
---

### What is the resolution of the "virtual" screen?

It's 320x200 pixels, stretched to fit the size of the current window. It is not
(currently) possible to change this from within Ruby scripts; edit the C source
code (specifically, the `sysInit()` function) and recompile if you really want.

This resolution was picked because it was used by games on the Amiga 500, Atari
ST, and Amstrad CPC 464.

The origin of the window (`0, 0`) is in the top-left corner.

### Some BMP files don't load at all, or are missing alpha data once loaded.

This is caused by a bug in older versions of SDL v2. Make sure you're using SDL
v2.0.4 or later - on Windows, simply replace the DLL files with newer versions.

### The `SDL_CreateRenderer()` call fails unless I add `SDL_RENDERER_SOFTWARE`.

I've seen this happen when running Windows Vista under QEMU with `-vga cirrus`,
though I'd guess that this happens with any "underpowered" video card, emulated
or not...

This is caused by a bug in older versions of SDL v2. Make sure you're using SDL
v2.0.4 or later - on Windows, simply replace the DLL files with newer versions.

### Which DLLs/packages do I need to run this?

On Windows, you'll need these DLLs on your system (or in the .EXE directory):

+ [SDL2](http://libsdl.org/)
+ [SDL2_image](http://www.libsdl.org/projects/SDL_image/)
+ [SDL2_mixer](http://www.libsdl.org/projects/SDL_mixer/)

...plus any additional DLLs needed for the audio files loaded by SDL mixer.

On Debian (or similar Linux-based systems), you'll need the following packages:

+ [libsdl2-2.0-0](https://packages.debian.org/sid/libsdl2-2.0-0)
+ [libsdl2-image-2.0-0](https://packages.debian.org/sid/libsdl2-image-2.0-0)
+ [libsdl2-mixer-2.0-0](https://packages.debian.org/sid/libsdl2-mixer-2.0-0)

I haven't tried running this on MacOSX, but it shouldn't be too dissimilar.

### How do I build this from source?

The simplest way is to install [Code::Blocks](http://www.codeblocks.org/) - you
can then open the `.cbp` project and compile it just as I do.

You'll need to have the following libraries available:

+ [libsdl2-dev](https://packages.debian.org/sid/libsdl2-dev)
+ [libsdl2-image-dev](https://packages.debian.org/sid/libsdl2-image-dev)
+ [libsdl2-mixer-dev](https://packages.debian.org/sid/libsdl2-mixer-dev)
+ [libmruby-dev](https://packages.debian.org/sid/libmruby-dev)

On Debian/Ubuntu/etc. you can simply install these via your package manager.

Windows/MacOSX versions of SDL2 are available from <http://libsdl.org/>; you'll
also need [SDL2_mixer](http://www.libsdl.org/projects/SDL_mixer/) for audio and
[SDL2_image](http://www.libsdl.org/projects/SDL_image/) for images).

The mruby project doesn't provide pre-built libraries; you need to compile from
source: <https://github.com/mruby/mruby/blob/master/doc/guides/compile.md>

For convenience, a pre-compiled version for Windows (32-bit) is provided here:
<https://gitlab.com/gobusto/catopuma/blob/048e7417b29c25c0575720606f15ea1e7a36ef7c/misc/mruby-1.2.0.zip>

Licensing
---------

Copyright (c) 2016 Thomas Glyn Dennis

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

### SDL2 licence

This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.

### mruby licence

Copyright (c) 2016 mruby developers

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
